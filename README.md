# 基于仓颉语言的简单 Server


## 特性
1. 支持 ioc 容器 (组件注解, 属性自动注入)
2. 支持自定义拦截器请求
3. 支持数据库查询和更新的工具类
4. 支持数据库查询返回对象自动映射
5. 支持切换数据源(当前基于 sqlite 轻量级数据库)

## 原理
### 容器初始化
1. 通过宏 Component (Controller, Service, Repository) 在编译器生成类的静态初始化器(通过导包自动加载)  
2. 在运行期, 注册 bean 对象时, 注入 bean 的属性 (查找失败, 不会报错)  
3. 在容器初始化完成后, 调用 AppContext#exposureBeans 初始化所有 bean (需要手工调用)  

### server 的接口分发 (SimpleDistributor)
1. 在第一次进行初始化, 查询 AppContext 中所有 bean 被 RequestMapping 注解修饰的成员函数  
2. 将被查询到的成员函数, 封装成可执行方法  
3. 根据路径和请求方式匹配可执行方法, 进行执行  


## 使用方法
[探索 SSM 框架在仓颉上的应用](./ssm.md)  

### 配置扫描
1. 扫描所有 bean 并注册  
2. 在注册完成后, 初始化所有未完成的 bean 对象  
`config/scan_packages.cj`
```
import simple_server.ioc.context.AppContext
// 导入需要被扫描的包
import simple_server.samples.service.*
import simple_server.samples.api.*
import simple_server.samples.config.initialization.*

// 初始化完成
class AfterInitializationComponent {
    static init() {
        AppContext.exposureBeans()
    }
}
```
### 配置数据源
```cangjie
/**
 * 数据源配置类
 */
@Component
public class DataSourceInfo <: OrmDataSourceInfo {
    public init() {
        this.driverName = "sqlite"
        this.url = "sqlite://web.db"
    }
}
```
### 编写 Controller
1. 导入当前包所需的注解, 容器, 工具类等  
`api/_import.cj`
```
internal import net.http.HttpContext
internal import std.log.Logger
internal import simple_server.ioc.annotations.Inject
internal import simple_server.ioc.context.AppContext
internal import simple_server.web.annotations.RequestMapping
internal import simple_server.web.annotations.RequestMethod
internal import simple_server.util.LogUtil
internal import simple_server.util.JsonUtil
internal import simple_server.util.ResponseUtil
```
1. 使用 Controller 注解注册到容器, 使用 Inject 注解自动注入对象  
2. 当请求 POST /user/list, 使用 UserController#list 成员函数来处理请求  
```
import simple_server.ioc.macros.Controller
import simple_server.samples.service.UserService

@Controller
@RequestMapping["/user"]
public class UserController {
    private var _userService = Option<UserService>.None
    @Inject
    public mut prop userService: UserService {
        get() {
            return this._userService.getOrThrow()
        }
        set(val) {
            this._userService = val
        }
    }

    // 查询数据库用户列表
    @RequestMapping["/list"]
    public func list(ctx: HttpContext) {
        let list = userService.listUser()
        ResponseUtil.jsonContent(ctx.responseBuilder).body(JsonUtil.toJson(list)).build()
    }
    // 初始化数据库用户信息
    @RequestMapping["/init"]
    public func initUserList(ctx: HttpContext) {
        userService.initUserList()
        ResponseUtil.jsonContent(ctx.responseBuilder).body(#"{"status": "success"}"#).build()
    }
}
```

### 编写 Service 和 Repository
导包同编写 Controller  
```cangjie
import std.collection.ArrayList
import simple_server.ioc.macros.Service
import simple_server.samples.entity.User

@Service // 交由容器管理
public class UserService {
    public func listUser(): ArrayList<User> {
        return userRepository.selectList()
    }
}
```
```cangjie
@Repository // 交由容器管理
public class UserRepository {
    /**
     * 查询数据
     */
    public func selectList(): ArrayList<User> {
        // 查询语句 
        let sql = "select id, name, role_id from user"
        // 查询参数
        let params: Array<SqlDbType> = []
        // 查询结果类型
        let resultTypes: Array<SqlDbType> = [SqlBigInt(0), SqlVarchar(""), SqlBigInt(0)]
        return DbUtil.query<User>(sql, params, resultTypes) {
            // 将查询结果转换对象
            let user = User()
            user.id = (resultTypes[0] as SqlBigInt).getOrThrow().value
            user.name = (resultTypes[1] as SqlVarchar).getOrThrow().value
            user.roleId = (resultTypes[2] as SqlBigInt).getOrThrow().value
            return user
        }
    }
    /**
     * 查询数据(结果集自动映射)
     */
    public func selectListAutoMapping(): ArrayList<User> {
        // 查询语句 
        let sql = "select id, name, role_id from user"
        // 查询参数
        let params: Array<SqlDbType> = []
        // 执行查询, 并将结果自动映射到 User 对象中(需实现 MappingEntity 接口)
        return DbUtil.query<User>(sql, params)
    }
}
```