# SSM 框架在仓颉上的应用


记录在仓颉实现类 SSM web 框架中与 Java 对比、遇到的问题和处理方案


### 环境

Cangjie Compiler: 0.53.4 (cjnative)
Target: x86_64-w64-mingw32


## Spring

参考: `src/ioc`

### 控制反转（IoC）

**bean 对象发现**

**Java：** 使用注解 Component, 在容器启动时扫描指定包, 构造 bean 对象信息，生成 bean 对象, 交由容器管理


**仓颉：** 使用宏 Component, 在展开宏时, 自动生成全局变量或者类的静态初始化块; 通过 `import` 导入包时, 自动收集 bean 对象信息

在容器启动时, 根据 bean 对象信息，生成 bean 对象, 交由容器管理


### 依赖注入（DI）

**bean 对象注入**

都可以通过反射来字段设置值

**注意**：仓颉中无法给 struct 设置值, 无法获取类型为枚举的返回值或者列表参数

(String 类型是 struct )(对此类型的操作，可定义接口，通过宏展开给字段赋值)



### 面向切面编程（AOP）

在仓颉中可使用宏在编译器拦截类(暂未深入)



## SpringMVC

参考: `src/web`

仓颉标准库支持 Server

可自定义 HttpRequestDistributor 来实现路由的分发  

通过 IoC 容器的对象结合注解, 自动生成分发接口


## Mybatis

单表操作参考框架: [dataORM4cj](https://gitcode.com/Cangjie-TPC/dataORM4cj/overview)

参考: `src/orm`

在仓颉中基于 CDBC 接口，对于查询结果的字段自动映射，通过反射设置到返回对象中

**注意**：仓颉中无法给 struct 设置值, 无法获取类型为枚举的返回值或者列表参数

(String 类型是 struct )(对此类型的操作，可定义接口，通过宏展开给字段赋值)



## 总结

无法通过反射实现，可通过宏+接口来完成